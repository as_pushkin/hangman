import re

from .word import Word

class Game:

    def __init__(self,
                 word,
                 alphabet = 'English',
                 tries = 11):
        self._word   = Word(word, alphabet)
        self._tries  = tries
        self._so_far = set()

    @property
    def word(self):
        return self._word

    @property
    def tries(self):
        return self._tries

    @tries.setter
    def tries(self, num):
        self._tries = num

    @property
    def so_far(self):
        return self._so_far

    def lost(self):
        return self.tries <= 0

    def won(self):
        return self.tries > 0 and self.word.guessed()

    def end(self):
        return self.lost() or self.won()

    def attempt(self, char):
        if char in self.so_far:
            return 0
        self.so_far.add(char)
        guessed = self._word.guess(char)
        if guessed == 0:
            self.tries -= 1;

        return guessed
