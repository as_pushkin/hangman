from .alphabet import Alphabet

class Word:

    def __init__(self,
                 word,
                 alphabet):
        self._word = word

        if type(alphabet) == str:
            self._alphabet = Alphabet(alphabet=alphabet)
        elif type(alphabet) == dict:
            self._alphabet = Alphabet(chars=alphabet)

        self._chars = dict()
        self._list = list()
        for i in range(len(word)):
            c = word[i]
            if self._alphabet.letter_exists(c):
                if c in self._chars:
                    self._chars[c].append(i)
                else:
                    self._chars[c] = [i]
                self._list.append('*')
            else:
                self._list.append(c)

        if len(self._chars) < 1:
            raise NoCharsFromAlphabet(self._word, self._alphabet)


    @property
    def word(self):
        return ''.join(self._list)

    @property
    def alphabet(self):
        return self._alphabet

    def guess(self, char):
        exists = 0
        for c in [char, self.alphabet.toggle_case(char)]:
            if c in self._chars:
                for i in self._chars[c]:
                    exists += 1
                    self._list[i] = c
                self._chars.pop(c)

        return exists

    def guessed(self):
        return not self._chars

# ==================================================== #

# Exceptions
class Error(Exception):
    pass


class NoCharsFromAlphabet(Error):
    def __init__(self,
                 word,
                 alphabet):
        self._word = word
        self._alphabet = alphabet
        print("No chars from '"
              + word
              + "' in",
              alphabet.all_chars.keys())
