import string
import re

alphabets = {
    'english':tuple(string.ascii_lowercase),
    'ENGLISH':tuple(string.ascii_uppercase),
}

known_alphabets = set(x.capitalize() for x in alphabets.keys())

class Alphabet:

    def __init__(self,
                 alphabet = None,
                 chars    = None):

        # Get predefined alphabet
        if alphabet is not None:
            a_lower = alphabet.lower()
            if a_lower not in alphabets:
                raise UnknownAlphabetError(alphabet)
            a_upper = alphabet.upper()
            if a_upper not in alphabets:
                raise UnknownAlphabetError(alphabets)
            self._lower = alphabets[a_lower]
            self._upper = alphabets[a_upper]
        # Provide lphabet as a dictionary:
        #    {
        #        'lower':'абвгд',
        #        'upper':'АБВГД',
        #    }
        # or
        #    {
        #        'lower':('а','б','в','г','д'),
        #        'upper':('А','Б','В','Г','Д'),
        #    }
        # or even
        #    {
        #        'lower':('а','б','в','г','д'),
        #        'upper':'АБВГД',
        #    }
        elif chars is not None:
            self._lower = tuple(chars['lower'])
            self._upper = tuple(chars['upper'])
        else:
            raise ValueError('Need to provide with alphabet name',
                             'or alphabet definition')

        # Lengths of upper and lower should be the same
        if len(self._lower) != len(self._upper):
            raise BadAlphabetDefinitionError(self._lower, self._upper)


        # Create alphabet dictionaries
        self._lower2upper = dict()
        self._upper2lower = dict()
        for i in range(len(self._lower)):
            self._lower2upper[self._lower[i]] = self._upper[i]
            self._upper2lower[self._upper[i]] = self._lower[i]

        # There must not be repeats
        if len(self._lower2upper) != len(self._upper2lower):
            raise BadAlphabetDefinitionError(self._lower2upper,
                                             self._upper2lower)

        self._all = {**self._lower2upper, **self._upper2lower}


    @property
    def all_chars(self):
        return self._all

    def letter_exists(self, char):
        return char in self.all_chars


    def toggle_case(self, char):
        if self.letter_exists(char):
            return self.all_chars[char]
        return None


# ================================================== #

# Exceptions
#
class Error(Exception):
    '''Parent of all exceptions'''
    pass
        

class UnknownAlphabetError(Error):

    def __init__(self, alphabet):
        super().__init__()
        self.alphabet = alphabet


class BadAlphabetDefinitionError(Error):

    def __init__(self, lower, upper):
        super().__init__()
        self._lower = lower
        self._upper = upper
