#!/usr/bin/env python

import os
import sys
dir_path = os.path.dirname(os.path.realpath(__file__))
sys.path.append( dir_path + '/lib')

import argparse
import jsonpickle
import json

from hangman.game import Game


def dump(obj):
    serialized = jsonpickle.encode(obj)
    print(json.dumps(json.loads(serialized), indent=4))

parser = argparse.ArgumentParser()
parser.add_argument("-w", "--word",
                    help='A word to guess',
                    type=str,
                    required=True)
parser.add_argument("-a", "--alphabet",
                    help='Alphabet to use',
                    default="English")

args = parser.parse_args()

word = args.word
alphabet = args.alphabet


game = Game(word, alphabet)
#dump(game.word())
#game = Game('Hello', {
#    'lower':'abcdefgh',
#    'upper':'ABCDEFGH'})

while not game.end():
    print(game.word.word)
    print("{} attempt(s) left".format(game.tries))
    if game.so_far:
        print("You have already tried: " +
              ','.join(sorted(game.so_far)))
    char = input("Try next letter: ")
    guessed = game.attempt(char)
    print("guessed: {}".format(guessed))
    if guessed > 0:
        message = "Congrats! There "
        ending  = ""
        if guessed == 1:
            message += "is"
        else:
            message += "are"
            ending = "s"
        message += " {} character{} in the word".format(guessed, ending)
        print(message)

if game.won():
    print("You won!")
elif game.lost():
    print("You lost...")
else:
    print("Something weird has happened...")
